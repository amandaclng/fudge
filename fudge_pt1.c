/*
 * fudge_pt1.c 
 *
 * The calling syntax is:
 *
 *		[del,K] = fudge_pt1(wrapped_phase);
 *
 * This is a MEX-file for MATLAB.
 *
 * Compile with mex fudge_pt1.c -lpthread
 *    
*/

#include "mex.h"
#include "math.h"
#include "pthread.h"
#include "errno.h"
#include "string.h"

struct thread_args {
	int startidx;
	int endidx;
    double *pha;
    const mwSize* dims;
    double *grad1,*grad2,*grad3;
    double *del2;
    double *K;
    double *w;
};

#define	NTHREADS 100
#define PI 3.141592653589793

void* compute_grad(void *args) {

    struct thread_args *data;
    
    double *pha;
    const mwSize* dims;
    
    int idx,i,j,k,o1,o2;
    double tmp;
    double k0,k1,k2;
    
    data = (struct thread_args*) args;
    
    pha = data->pha;
    dims = data->dims;

    k0 = PI/dims[0];
    k1 = PI/dims[1];
    k2 = PI/dims[2];
    
    o1 = dims[0]; o2 = dims[0]*dims[1];
	k = data->startidx/o2;
	j = (data->startidx - k*o2)/o1;
	i = data->startidx - k*o2 - j*o1;
    for (idx = data->startidx; idx < data->endidx; idx++) {
    	
        if (i < dims[0]-1) {
            data->grad1[idx] = atan2(sin(pha[idx+1] - pha[idx]),cos(pha[idx+1] - pha[idx]));
        }
        else {
            data->grad1[idx] = 0;
        }        
     
        if (j < dims[1]-1) {
            data->grad2[idx] = atan2(sin(pha[idx + o1] - pha[idx]),cos(pha[idx + o1] - pha[idx]));
        }
        else {
            data->grad2[idx] = 0;
        }        
        
        if (k < dims[2]-1) {
            data->grad3[idx] = atan2(sin(pha[idx + o2] - pha[idx]),cos(pha[idx + o2] - pha[idx]));
        }
        else {
            data->grad3[idx] = 0;
        }        
  
  		data->del2[idx] = data->grad1[idx] + data->grad2[idx] + data->grad3[idx];
  		
  		if (data->K) {
	  		data->K[idx] = -6 + 2*(cos(k0 * i) + cos(k1 * j) + cos(k2 * k) );
	  	}
  		
  		i++;
  		if (i == dims[0]) {
  			i = 0;
  			j++;
  			if (j == dims[1]) {
  				j = 0;
  				k++;
  			}
  		}
    }
    
	pthread_exit(NULL);
/*	return NULL; */
}

void* compute_del(void *args) {

    struct thread_args *data;
    
    double *pha;
    const mwSize* dims;
    double *grad1,*grad2,*grad3;
    double *del2;
    
    int idx,i,j,k,o1,o2;
    double tmp;
    
    data = (struct thread_args*) args;
    
    pha = data->pha;
    dims = data->dims;
    grad1 = data->grad1;
    grad2 = data->grad2;
    grad3 = data->grad3;
    del2 = data->del2;
        
    o1 = dims[0]; o2 = dims[0]*dims[1];
	k = data->startidx/o2;
	j = (data->startidx - k*o2)/o1;
	i = data->startidx - k*o2 - j*o1;
    for (idx = data->startidx; idx < data->endidx; idx++) {
        
        if (i > 0) {
            del2[idx] -= grad1[idx-1];
        }
        if (j > 0) {
            del2[idx] -= grad2[idx-o1];
        }
        if (k > 0) {
            del2[idx] -= grad3[idx-o2];
        }
        
  		i++;
  		if (i == dims[0]) {
  			i = 0;
  			j++;
  			if (j == dims[1]) {
  				j = 0;
  				k++;
  			}
  		}
    } 

	pthread_exit(NULL);
/*	return NULL; */
}

void fudge(double* pha, double* phi, double* K, mwSize N, const mwSize* dims) {

	int idx,i;
    double *grad1,*grad2,*grad3;
    double tmp,old_tmp;
    
    pthread_t thread[NTHREADS];
    struct thread_args args[NTHREADS];
    void*  status;
    
    grad1 = (double*) malloc(dims[0]*dims[1]*dims[2] * sizeof(double));
    grad2 = (double*) malloc(dims[0]*dims[1]*dims[2] * sizeof(double));
    grad3 = (double*) malloc(dims[0]*dims[1]*dims[2] * sizeof(double));
    
	idx = 0;
	for (i = 0; i < NTHREADS; i++) {

		args[i].startidx = idx;
		args[i].endidx = i==NTHREADS-1 ? dims[0]*dims[1]*dims[2] : idx + dims[0]*dims[1]*dims[2]/NTHREADS;
		idx = args[i].endidx;
		args[i].pha = pha;
		args[i].dims = dims;
		args[i].grad1 = grad1;
		args[i].grad2 = grad2;
		args[i].grad3 = grad3;
		args[i].del2 = phi;
		args[i].K = K;
		
		if (pthread_create( &thread[i], NULL, compute_grad, (void*) &args[i] ) != 0) {
			fprintf(stderr, "pthread_create(compute_grad) failed errno = %d, %s\n", errno, strerror(errno));
			}
	}
  
  	for (i=0; i < NTHREADS; i++) {
	  	pthread_join(thread[i],&status);	
	}
  
	for (i = 0; i < NTHREADS; i++) {
		if (pthread_create( &thread[i], NULL, compute_del, (void*) &args[i] ) != 0) {
			fprintf(stderr, "pthread_create(compute_del) failed errno = %d, %s\n", errno, strerror(errno));
			}
	}
  
  	for (i=0; i < NTHREADS; i++) {
	  	pthread_join(thread[i],&status);	
	}

    free(grad1);
    free(grad2);
    free(grad3);  
    
  	/* 
     * DO NOT CALL pthread_exit(NULL) HERE
  	 * It causes Matlab to hang.   
     */  
}

/* The gateway function */
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
	/* ---- DECLARE VARIABLES ---- */
	double        *pha; 		/* input array (wrapped phase) */
	mwSize        N;			/* number of dimensions of input array */
	const mwSize* dims;     	/* size of input array */
	double        *del2, *K; 	/* output arrays (del2 = laplacian of phase, 
	                    		 *   K = coefficients for inverse laplacian 
                                 *   operation) */
	/* ---- VALIDATE INPUTS ---- */
	if(nrhs!=1) {
		mexErrMsgIdAndTxt("fudge:nrhs", "One input required.");
	}

	if(nlhs!=1 && nlhs!=2) {
		mexErrMsgIdAndTxt("fudge:nlhs", "One or two outputs required.");
	}

	if( !mxIsDouble(prhs[0]) ) {
		mexErrMsgIdAndTxt("fudge:notDouble", "Input must be type double.");
	}

	if ( mxIsComplex(prhs[0]) ) {
		mexErrMsgIdAndTxt("fudge:notReal", "Input must be real.");
	}

    /* 
     * TODO 
     * Check is array
     * Handle singles
     * Handle one and two dimensions
     */

	/* ---- INITIATE VARIABLES ---- */

	pha = mxGetPr(prhs[0]); /* get pointer to unwrapped phase double array */
	N = mxGetNumberOfDimensions(prhs[0]); /* get num of dimensions of input array */
	dims = mxGetDimensions(prhs[0]); /* get the size of input array */
    
    if (N != 3) {
        mexErrMsgIdAndTxt("fudge:not3D", "Input must be 3D.");
    }
    
    if (dims[0] < 2 | dims[1] < 2 | dims[2] < 2) {
        mexErrMsgIdAndTxt("fudge:not3D", "Input must be 3D.");
    }
    
    plhs[0] = mxCreateNumericArray(N, dims, mxDOUBLE_CLASS, mxREAL); /* create output array (del2) */
	del2 = mxGetPr(plhs[0]); /* get pointer to del2 double array */

	if (nlhs == 2) { /* if a second output (K) is requested ... */
		plhs[1] = mxCreateNumericArray(N, dims, mxDOUBLE_CLASS, mxREAL); /* create output array */
		K = mxGetPr(plhs[1]); /* get pointer to K double array */
	}
	else { /* if no second output is requested, set K to NULL */
		K = NULL;
	}
	
	/* ---- RUN FUDGE_PT1 ON INPUT ARRAY ---- */
	
	fudge(pha,del2,K,N,dims);
}
