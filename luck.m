function phi = luck(pha,symmetric)
% unwrapped_phase = luck(wrapped_phase, symmetric)
%
% symmetric = true/false. True (default) runs LUCK using symmetrically
% padded unwrapped phase. This is mathematically more accurate, but takes
% a lot more time and memory.

    narginchk(1,2);
    nargoutchk(1,1);
    
    if ~isnumeric(pha) || isscalar(pha) || ndims(pha) > 4
        error 'Unwrapped phase must be a 1D, 2D or 3D array'
    end

    sz = size(pha);

    if ndims(pha) == 3
        dims = 3;
    elseif isvector(pha)
        dims = 1;
    else
        dims = 2;
    end
    
    if nargin == 1 || isempty(symmetric)
        symmetric = true;
        pha = padarray(pha, sz.*(sz>1),'symmetric', 'post');
    else
        symmetric = false;
    end

    L = zeros(size(pha));
    switch dims
        case 3
            L(1,1,1) = -6; 
            L(1,1,2) = 1; L(1,1,end) = 1;
            L(1,2,1) = 1; L(1,end,1) = 1;
            L(2,1,1) = 1; L(end,1,1) = 1;
        case 2
            L(1,1) = -4;
            L(1,2) = 1; L(1,end) = 1;
            L(2,1) = 1; L(end,1) = 1;
        otherwise
            L(1) = -2;
            L(2) = 1;
            L(end) = 1;
    end
    L = fftn(L);

    t = sin(pha);
    t = -4*pi^2 .* ifftn(L .* fftn(t));
    t1 = cos(pha) .* t;

    t = cos(pha);
    t = -4*pi^2 .* ifftn(L .* fftn(t));
    t2 = sin(pha).*t;

    phi = fftn(t1 - t2) ./ L;
    phi(isinf(phi)|isnan(phi)) = 0;
    phi = real(-(1/(4*pi^2)) .* ifftn(phi));
    if symmetric
        switch dims
            case 3
                phi = phi(1:sz(1),1:sz(2),1:sz(3));
                phi = phi - phi(floor(sz(1)/2), floor(sz(2)/2),floor(sz(3)/2));
            case 2
                phi = phi(1:sz(1),1:sz(2));
                phi = phi - phi(floor(sz(1)/2), floor(sz(2)/2));
            otherwise
                phi = phi(1:numel(phi)/2);
                phi = phi - phi(floor(numel(phi)/2));
        end
    end
    
    pt = num2cell(ceil(sz/2),1);
    phi = phi - phi(pt{:}) + pha(pt{:});
    
