function [del,K] = fudge_pt1(wrapped_phase)
% [del,K] = fudge_pt1(wrapped_phase)
%
% fudge_pt1 is the first part of the FUDGE phase unwrapping algorithm. It
% calculates the Laplacian of the true phase (del) from the wrapped phase.
% 
% Inputs: 
%   wrapped_phase: 3D double array
%
% Outputs:
%   del: Laplacian of the unwrapped phase
%   K: discrete cosine transform of the Laplacian operator
