function b=dctn(arg1,varargin)
%DCTN N-D discrete cosine transform.
%   B = DCTN(A) returns the discrete cosine transform of A.
%   The array B is the same size as A and contains the
%   discrete cosine transform coefficients.
%
%   B = DCTN(A,[M N ...]) or B = DCTN(A,M,N,...) pads the array A with
%   zeros to size M-by-N-by-... before transforming. If the sizes are
%   smaller than the corresponding dimension of A, DCTN truncates
%   A. 
%
%   This transform can be inverted using IDCTN.
%
%   Class Support
%   -------------
%   A can be numeric or logical. The returned matrix B is of 
%   class double.
%
%   See also DCT2, IDCT2, IDCTN.

%   Created 18 March 2015 by Amanda Ng - based on DCT2
%   Amended 31 July 2015 by Amanda Ng - Fixed problem with vectors

% Parameter check
narginchk(1,Inf);
nargoutchk(1,1);

if nargin > 1
    if numel(varargin) > 1
        if any(cellfun(@(x) ~isscalar(x) || ~isint(x) || x <= 0, varargin))
            error 'Input error: parameters 2,3,... must be scalar positive integers'
        end
        newsz = cell2mat(varargin);
    else
        newsz = varargin{1};
        if ~isvector(newsz) || numel(newsz) ~= ndims(arg1)
            error 'Input error: second parameter is not a vector or number of elements does not equal number of dimensions of A'
        end
    end

    % Pad/truncate array as necessary
    sz = size(arg1);
    p = newsz-sz;
    p(p<0) = 0;
    arg1 = padarray(arg1,p,0,'post');

    p = arrayfun(@(x) 1:x,newsz,'UniformOutput',false);
    arg1 = arg1(p{:});
end

% Basic algorithm.
b = arg1;
if isvector(arg1)
    b = dct(arg1);
else
    N = ndims(arg1);
    for n = 1:N
        p = [n 2:N];
        p(n) = 1;
        tmp = permute(b,p);
        sz = size(tmp);
        b = permute(reshape(dct(reshape(tmp,sz(1),[])),sz),p);
    end
end