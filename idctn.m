function b = idctn(arg1,varargin)
%IDCTN 2-D inverse discrete cosine transform.
%   B = IDCTN(A) returns the two-dimensional inverse discrete
%   cosine transform of A.
%
%   B = IDCTN(A,[M N ...]) or B = IDCTN(A,M,N,...) pads A with zeros (or
%   truncates A) to create a array of size M-by-N-by-... before
%   transforming. 
%
%   For any A, IDCTN(DCTN(A)) equals A to within roundoff error.
%
%   The discrete cosine transform is often used for image
%   compression applications.
%
%   Class Support
%   -------------
%   The input array A can be of class double or of any
%   numeric class. The output array B is of class double.
%
%
%   See also DCTN.

%   Created 18 March 2015 by Amanda Ng - based on IDCT2
%   Amended 31 July 2015 by Amanda Ng - Fixed problem with vectors

% Parameter check
narginchk(1,Inf);
nargoutchk(1,1);

if nargin > 1
    if numel(varargin) > 1
        if any(cellfun(@(x) ~isscalar(x) || ~isint(x) || x <= 0, varargin))
            error 'Input error: parameters 2,3,... must be scalar positive integers'
        end
        newsz = cell2mat(varargin);
    else
        newsz = varargin{1};
        if ~isvector(newsz) || numel(newsz) ~= ndims(arg1)
            error 'Input error: second parameter is not a vector or number of elements does not equal number of dimensions of A'
        end
    end

    % Pad/truncate array as necessary
    sz = size(arg1);
    p = newsz-sz;
    p(p<0) = 0;
    arg1 = padarray(arg1,p,0,'post');

    p = arrayfun(@(x) 1:x,newsz,'UniformOutput',false);
    arg1 = arg1(p{:});
end

% Basic algorithm.
b = arg1;
if isvector(arg1);
    b = idct(arg1);
else
    N = ndims(arg1);
    for n = 1:N
        p = [n 2:N];
        p(n) = 1;
        tmp = permute(b,p);
        sz = size(tmp);
        b = permute(reshape(idct(reshape(tmp,sz(1),[])),sz),p);
    end
end