function phi = fudge(pha)
% UNWRAPPED = FUDGE(WRAPPED)
%
% FUDGE unwraps phase data.

    narginchk(1,1);
    nargoutchk(1,1);

    if ~isnumeric(pha) || isscalar(pha) || ndims(pha) > 4
        error 'Unwrapped phase must be a 1D, 2D or 3D array'
    end
    
    sz = size(pha);
    
    if ndims(pha) == 3
        dims = 3;
    elseif isvector(pha)
        dims = 1;
    else
        dims = 2;
    end
    
    switch dims
        case 3
            if ~isa(pha,'double')
                error 'Input must be of type double'
            elseif ~isreal(pha)
                error 'Input must be real'
            end
            [del,K] = fudge_pt1(pha);
        case 2
            grad1 = padarray(diff(pha,1,1),[1 0 0],'post');
            grad2 = padarray(diff(pha,1,2),[0 1 0],'post');
            del = convn(atan2(sin(grad1),cos(grad1)),reshape([0 1 -1],[3 1 1]),'same') + ...
                  convn(atan2(sin(grad2),cos(grad2)),reshape([0 1 -1],[1 3 1]),'same');

            [K1,K2] = ndgrid(0:sz(1)-1,0:sz(2)-1);
            K = (-2+2*cos(pi/sz(1) .* K1)) + ...
                (-2+2*cos(pi/sz(2) .* K2));
        otherwise
            pha = reshape(pha,1,[]);
            grad1 = padarray(diff(pha,1,2),[0 1 0],'post');
            K = -2+2*cos(pi/size(pha,2) .* (0:size(pha,2)-1));
            del = conv(atan2(sin(grad1),cos(grad1)),[0 1 -1],'same');
    end
            
    phi = dctn(del) ./ K;
    phi(isinf(phi)|isnan(phi))=0;
    phi = idctn(phi);
    
    switch dims
        case 3
            phi = phi - phi(floor(sz(1)/2), floor(sz(2)/2),floor(sz(3)/2)) + pha(floor(sz(1)/2), floor(sz(2)/2),floor(sz(3)/2));
        case 2
            phi = phi - phi(floor(sz(1)/2), floor(sz(2)/2)) + pha(floor(sz(1)/2), floor(sz(2)/2));
        otherwise
            phi = reshape(phi,sz) - phi(floor(numel(phi)/2)) + pha(floor(numel(pha)/2));
    end
            
    


    
