This package contains the Matlab files for running the Fast Unwrapping with Discrete Gradient Evaluation (FUDGE) method of MRI phase unwrapping.

In order to run this function you will need to compile the fudge_pt1.c code using (in Matlab):

mex fudge_pt1.c -lpthread

To unwrap phase data, use the command:

unwrapped = fudge(wrapped)

where 'wrapped' is the wrapped phase data.

